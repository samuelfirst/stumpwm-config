#!/bin/bash

# Autopopulate the run control files by moving them to the home dir,
# then calling re-link.sh
mv -f ./stumpwmrc ~/.stumpwmrc
mv -rf ./stumpwm.d ~/.stumpwm.d

./re-link.sh
