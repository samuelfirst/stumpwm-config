;;; Attempt to load a file.
;; If there's an error, try to log it and bail on loading the file.
;; This keeps a bit of bad code from breaking the window manager.
(defvar *load-error-log* nil)
(defvar *load-blacklist* (list #P"~/.stumpwm.d/pager.lisp"))
(defun safe-load (file)
  (let ((loadedp
	 (not (cadr
	       (multiple-value-list (unless (find file *load-blacklist*)
				      (ignore-errors
					(load file))))))))
    (unless loadedp
      (ignore-errors (load #P"~/.stumpwm.d/deps/logger.lisp"))
      (if (fboundp 'logger-log)
	  (if *load-error-log*
	      (append-log *load-error-log* (format nil "~A" file))
	    (progn
	      (setf *load-error-log* (make-instance 'logger))
	      (add-log "Load Error Log" *load-error-log*)
	      (append-log *load-error-log* (format nil "~A" file))))
	(setf *load-error-log* "Could not load logger"))
      (setf *startup-message* "Encountered errors loading files"))))

;;; Load all the files in ~/.stumpwm.d
;; Files from the deps subdirectory are loaded before everything else
;; so functions from them can be used in other files without worrying
;; about the load order.
;; Files that depend on other files should be put in the top level.
;; Files that implement trivial functions meant to be used by other
;; files should be placed in deps/.  If a file in deps/ needs to use
;; another dependency (for example, deps/extension.lisp), it can load
;; it directly.
(let ((load-dir (append
		 (directory #P"~/.stumpwm.d/deps/*.lisp")
		 (directory #P"~/.stumpwm.d/*.lisp"))))
  (unless (not load-dir)
    (loop for file in load-dir doing
	  (safe-load file))))

;;; Set prefix key to C-z instead of C-t
(set-prefix-key (kbd "C-z"))

;;; Configure the mode line
(setf *screen-mode-line-format*
      (list "[^Rgroup: %n^r]^B"
	    '(:eval (get-battery))
	    "^6[" '(:eval (get-volume))
	    "]^n^5[time: %d]^n^7[%w]^b^n"))
(setf *mode-line-timeout* 1)
(setf *window-format* "[%s|%50t]")
(enable-mode-line (current-screen)
		  (current-head) t)
