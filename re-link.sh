#!/bin/bash

# Cycle through target dir ($1), unlinking links
function unlink-dir {
    for i in "$1"/*
    do
	if [ -f "$i" ]
	then
	    unlink "$i"
	fi
    done
}

# Cycle through original dir ($1), creating hardlinks to each file in target dir
# ($2)
function link-dir {
    for i in "$1"/*
    do
	ln "$i" "$2"/$(basename "$i")
    done
}

# Unlink all the hardlinks
unlink ./stumpwmrc
unlink-dir ./stumpwm.d/deps
unlink-dir ./stumpwm.d

# Recreate all the hardlinks
ln ~/.stumpwmrc ./stumpwmrc
link-dir ~/.stumpwm.d ./stumpwm.d
link-dir ~/.stumpwm.d/deps ./stumpwm.d/deps
