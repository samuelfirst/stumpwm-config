;;; Rebind C-q to do nothing if the current window is a browser.
;; This will keep the browser from closing when I mis-type C-w.
(defcommand ctrl-q () ()
  (unless (equal (window-role (current-window)) "browser")
    (stumpwm::send-meta-key (current-screen) (kbd "C-q"))))
(define-key *top-map* (kbd "C-q") "ctrl-q")

;;; Switch the functions of (n, p) and (C-M-n, C-M-p)
(define-key *root-map* (kbd "n") "next-in-frame")
(define-key *root-map* (kbd "C-M-n") "pull-hidden-next")
(define-key *root-map* (kbd "p") "prev-in-frame")
(define-key *root-map* (kbd "C-M-p") "pull-hidden-prev")

;;; Set the f11 key to fullscreen the window
(define-key *top-map* (kbd "F11") "fullscreen")


;;; Bind C-z-= to 'balance-frames' so I don't have to press shift
(define-key *root-map* (kbd "=") "balance-frames")
