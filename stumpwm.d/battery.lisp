;;; Grab the current battery capacity from /sys/class/
(defun battery-capacity ()
  (with-open-file (stream "/sys/class/power_supply/BAT1/capacity")
		  (read-line stream nil)))

(defun battery-chargingp ()
  (with-open-file (stream "/sys/class/power_supply/BAT1/status")
		  (let ((stat (read-line stream nil)))
		    (if (string-equal stat "Charging")
			t
		      nil))))

;;; Get the battery and return a pretty string for the modeline
;; If it is charging, it sticks a spinner in the output
(defun get-battery ()
  (let* ((cap (battery-capacity))
	 (per (parse-integer cap)))
    (concatenate 'string (cond ((< per 25) "^1")
			       ((< per 50) "^3")
			       (t "^2"))
		 "["
		 (progress-bar 
		  (concatenate 'string
			       (format nil "battery: ~a%" cap)
			       (if (battery-chargingp)
				   (format nil " ~a"
					   (car (funcall *spin*)))))
		  per)
		 "]^n")))

