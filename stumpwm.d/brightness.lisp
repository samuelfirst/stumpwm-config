;;; Set up commands/bindings for the backlight up & down keys
(defun print-brightness ()
  (message "~A%"
	   (run-shell-command
	    "xbacklight -get | awk -F '.' '{print $1}' | head -c -1"
	    t)))

(defcommand brightness-down () ()
  (run-shell-command "xbacklight -steps 1 -time 0 -inc 1%")
  (print-brightness))

(defcommand brightness-up () ()
  (run-shell-command "xbacklight -steps 1 -time 0 -dec 1%")
  (print-brightness))

(define-key *top-map* (kbd "XF86MonBrightnessDown") "brightness-down")
(define-key *top-map* (kbd "XF86MonBrightnessUp") "brightness-up")
