;;;; The purpose of this file is to retrofit lisp implemenatations with
;;;; optional features they may be missing.

;;; Implement featurep unless it's already implemented
;; Searches for a feature in *features* and returns T if it
;; exits or NIL if it doesn't.
(unless (fboundp 'featurep)
  (defun featurep (feature)
    (loop for f in *features* doing
	  (if (equal f feature)
	      (return t)))))

;;; Implements proper-list-p unless it's already implemented
;; Checks whether a list is a proper list (ends with nil) by seeing
;; whether cadr-ing the last element throws an error.  This isn't the
;; cleanest way to implement it, but it is fairly compact.
(unless (fboundp 'proper-list-p)
  (defun proper-list-p (l)
    (not (cadr (multiple-value-list (ignore-errors (cddr (last l))))))))

;;; Add function to count the number of new lines in a string
(defun count-lines (s)
  (loop for c across s summing (if (string-equal c #\newline) 1 0)))

;;; Add function to flatten a multi-dimensional list
(defun flatten (lst)
  (let ((flat '()))
    (loop for i in lst doing
	  (cond ((listp i)
		 (setf flat (concatenate 'list flat (flatten i))))
		(T
		 (setf flat (concatenate 'list flat (cons i nil))))))
    flat))
