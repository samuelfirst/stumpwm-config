;; Set up progress spinner
(defun spinner (chars)
  (let ((s chars))
    (lambda ()
      (setf s (append (cdr s) (cons (car s) nil))))))

(setf *spin* (spinner '("o" "O" "0")))

(defcommand set-spinner () ()
  (setf *spin*
	(spinner (cdr (select-from-menu
		       (current-screen)
		       '(("Circle" . ("o" "O" "0"))
			 ("Bar" . ("|" "/" "-" "\\"))
			 ("Angled" . ("<" "^^" ">" "v"))
			 ("Wink" . (":)" ";)"))
			 ("Cylon" . ("*  " " * " "  *" " * "))))))))


;;; Set up progress bar
;; Inverts the text color to show what percentage has passed
;; Meant to be used with battery and volume
;; Expects a string to turn into the bar and an int for the fullness
;; Returns string with formatting for the stumpwm color engine
(defun progress-bar (text per)
  (let ((delim (floor (/ (* per (length text)) 100))))
    (concatenate 'string "^R"
		 (subseq text 0 delim)
		 ; There seems to be a bug with the modeline where if
		 ; you try to put a ^r after '%' it will eat the
		 ; '^'.  I guess it has a taste for carets ;)
		 ; The solution is to insert a non-printing character
		 ; before the '^' so it doesn't eat it.  Rubout is
		 ; semi-standard, but I think most CL implementations
		 ; support it.
		 (format nil "~C~A" #\Rubout "^r")
		 (subseq text delim))))
