;;; Wraps around group and windows to provide lists of both
;; As far as I can tell there's no built-in way to get this information,
;; so even though this isn't super clean, it should (mostly work).
;; The way they do it in grouplist sort of works, but only for the
;; current screen; I want to get all the groups, across screens.

(defvar *gnew-func* #'gnew)
(defvar *gkill-func* #'gkill)
(defvar *grename-func* #'grename)
(defvar *groups* (cons (current-group) nil))

(defun gnew (name)
  (funcall *gnew-func* name)
  (setf *groups* (append *groups* (cons (current-group) nil))))

;; The behavior of this is probably slightly different than the original
;; function, but since I never use it anyway, it doesn't really matter.
;; If I find myself needing to use the original in the future, I might
;; wrap it with a function named gnewbg-old or something to that effect.
;; Alternatively, I could try to make this function mimic the behavior
;; of the original function.
(defun gnewbg (name)
  (let ((old-group (current-group)))
    (gnew name)
    (switch-to-group old-group)))

(defun gkill ()
  (let ((g '()))
    (loop for i from 0 to (- (length *groups*) 1) doing
	  (if (equal (nth i *groups*) (current-group))
	      nil
	    (setf g (append g (cons (nth i *groups*) nil)))))
    (setf *groups* g))
  (funcall *gkill-func*))

(defun grename (name)
  (let ((g '())
	(old-group (current-group)))
    (funcall *grename-func* name)
    (loop for i from 0 to (- (length *groups*) 1) doing
	  (if (equal (nth i *groups*) old-group)
	      (setf g (append g (cons (current-group) nil)))
	    (setf g (append g (cons (nth i *groups*) nil)))))))

(defun build-window-list ()
  (let ((windows '()))
    (loop for group in *groups* doing
	  (setf windows (append windows
				(group-windows group))))
    windows))


;;; Extends the functionality for the built-in *destroy-window-hook* by
;; allowing different hooks to be set on a per-window basis.
(defvar *window-kill-hooks* '())
(defun add-window-kill-hook (window func)
  (setf *window-kill-hooks*
	(append *window-kill-hooks*
		'(window . func))))
;; TODO: Add clean-up for *window-kill-hooks* so it doesn't get filled
;; up with hooks for windows that have already been destroyed.
;(add-hook *destroy-window-hook*
;	  (lambda ()
;	    (let ((destroyed t)
;		  (hooks '()))
;	      (loop for hook in *window-kill-hooks* doing
;		    (loop for win in (build-window-list) doing
;			  (if (equal (car hook) win)
;			      (progn
;				(setf destroyed nil)
;				(break))))
;		    (if destroyed
;			(setf hooks
;			      (append hooks
;				      (cons (cdr hook) nil)))
;		      (setf destroyed t)))
;	      (loop for hook in hooks doing
;		    (funcall hook)))))
;
