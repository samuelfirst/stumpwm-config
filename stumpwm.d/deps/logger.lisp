;;; Set up logging facilities
;; Creates a generic logger class that can be:
;;  - Logged to
;;  - Printed
;;  - Written to a log file
;;  - Automatically rotated when it reaches its max size
;;    - Unless max size is a negative value; then it's infinite
(defclass logger ()
  ((log :initarg :log
	:accessor logger-log
	:initform '())
   (max-size :initarg :max-size
	     :accessor logger-max-size
	     :initform -1)
   (log-file :initarg :log-file
	     :accessor logger-file
	     :initform #P"~/.stumpwm.log")
   (log-format :initarg :log-format
	       :accessor logger-format
	       :initform "~A")
   (log-timestamp :initarg :timestamp
		  :accessor logger-timestamp
		  :initform T)))

(defun time-stamp (lis)
  (let* ((time (multiple-value-list (get-decoded-time)))
	 (seconds (nth 0 time))
	 (minutes (nth 1 time))
	 (hours (nth 2 time))
	 (day (nth 3 time))
	 (month (nth 4 time))
	 (year (nth 5 time)))
    (loop for i in lis collecting
	  (concatenate 'string
		       (format nil "~2@A, ~2@A ~4@A; ~2@A:~2@A:~2@A :: "
			       month day year hours minutes seconds)
		       i))))


(defmethod rotate-log ((log logger) (item t))
  (unless (minusp (logger-max-size log))
    (if (proper-list-p item)
	(if (> (+ (length (logger-log log)) (length item))
	       (logger-max-size log))
	    (setf (logger-log log)
		  (subseq (logger-log log) (length item))))
      (if (> (+ (length (logger-log log)) 1) (logger-max-size log))
	  (setf (logger-log log) (subseq (logger-log log) 1))))))

(defmethod append-log ((log logger) (item t))
  (rotate-log log item)
  (let ((message (if (proper-list-p item)
		     item
		   (cons item nil))))
    (setf (logger-log log)
	  (append (logger-log log)
		  (if (logger-timestamp log)
		      (time-stamp
		       (loop for i in message collecting
			     (format nil (logger-format log) i)))
		    (loop for i in message collecting
			  (format nil (logger-format log) i)))))))

(defmethod print-log ((log logger))
  (message (format nil "~{~A~%~}" (logger-log log))))


;;; TODO: Add in deduplication.
(defmethod write-log ((log logger))
  (let ((l (logger-log log)))
    (let ((idx 0))
      (with-open-file (stream (logger-file log)
			      :direction :input
			      :if-does-not-exist :create)
		      (loop as line = (read-line stream nil nil)
			    while (not (equal line nil)) doing
			    (unless (find line l :test #'string=)
			      (setf l
				    (append
				     (subseq l 0 idx)
				     (cons line nil)
				     (nthcdr idx l)))
			      (1+ idx)))))
    (with-open-file (stream (logger-file log)
			    :direction :output
			    :if-does-not-exist :create
			    :if-exists :supersede)
		    (format stream "~{~A~%~}" l))))

;;; Command to select log to print from list of logs
(defvar *logs* '())
(defun add-log (name log)
  (setf *logs*
	(append *logs* (cons (cons name log) nil))))

;;; Creates a global log variable and runs add-log over it
(defmacro deflog (var name)
  `(defvar ,var (make-instance 'logger))
  `(add-log ,name ,var))

(defcommand get-log () ()
  (if *logs*
      (print-log (cdr (select-from-menu (current-screen) *logs*)))
    (message "No logs")))
