;;;; This implements a basic pager in stumpwm to allow for scrollable
;;;; messages.
;(load "~/.stumpwm.d/deps/extension.lisp")
;
;;;; Displays as much of a string as it can in a message and waits for
;;; user input to scroll the message.
(defun message-subseq (str start stop)
  (let ((string-split (split-string str (format nil "~%"))))
    (message "~{~A~%~}" (subseq string-split start stop))))

(defcommand test-msg-subseq () ()
  (message-subseq (format nil "a~%b~%c~%d~%") 1 3))
