;;; Start steam in a new group
;; Steam needs to be quarantined to its own group because it doesn't
;; like to play nice with splitting.
;; The linux version of steam also doesn't support automatically
;; launching the client in offline mode, so this retrofits it with that
;; functionality by pinging the steam store, and adding options to
;; steam.cfg to force it offline if the ping fails.  After steam exits,
;; steam.cfg is removed.
;; Currently the ping is done through run-shell-command.  In the future,
;; I'd like to implement a proper ping function using sockets.  The
;; version of SBCL that I'm using (1.4.5), however, does not support
;; them.
(defcommand steam () ()
  (let ((onlinep
	 (search "1 received"
		 (run-shell-command
		  "ping -c 1 store.steampowered.com" t))))
    (unless onlinep
       (with-open-file (stream #P"~/.steam/steam.cfg"
			       :direction :output
			       :if-does-not-exist :create)
		       (format stream "~A~%~A~%~A"
			       "BootStrapperInhibitAll=enable"
			       "BootStrapperForceSelfUpdate=disable"
			       "ForceOfflineMode=enable")))
    (gnew "steam")
    (run-shell-command "steam ; rm -f ~/.steam/steam.cfg")))
