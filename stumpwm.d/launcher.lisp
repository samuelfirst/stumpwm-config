;;; Separates the program name from a string
;; Program strings will sometimes contain arguments to pass to the
;; program.  In order to search for files assosciated with the program,
;; the name needs to be parsed out.
;; There's a fast/dirty hack in this where I slap a space on the end of
;; 'program-string'.  This is to stop it from biting the last char off
;; of strings that don't have any spaces.
(defun separate-program (program-string)
  (coerce
   (butlast (loop for c across
		  (concatenate 'string program-string " ")
		  collecting c until (char-equal c #\space)))
   'string))

;;; Loop over a list, un-namestring-ing each item
(defun namestring-list (lst)
  (loop for p in lst collecting
	(car (last (split-string (namestring p) "/")))))

;;; Creates a list of programs in each directory in $PATH
(defun enumerate-path ()
  (let* ((paths (split-string (sb-unix::posix-getenv "PATH") ":"))
	 (progs (flatten (loop for dir in paths collecting
			       (directory
				(concatenate 'string dir "/*")
				:resolve-symlinks nil)))))
    (namestring-list progs)))

;;; Creates list of completions for programs and stumpwm commands
;; If the directory /tmp/bin-overlay exists, we can skip calling
;; enumerate-path and just reference that.  This should reduce the
;; number of cycles spent collecting programs.  The following entry
;; can be added to fstab to automount the union (replace $PATH with
;; the content of the PATH environment variable):
;;
;;   overlayfs /tmp/bin-overlay overlayfs lowerdir=$PATH,nofail 0 0
;;
;; NOTE: Make sure when placing the above in fstab to verify that all
;; the directories actually exist, otherwise it will fail.
(defun get-completions ()
  (let ((union (namestring-list (directory "/tmp/bin-overlay/*"))))
    (cond
     (union
      (flatten (list union (stumpwm::all-commands))))
     (T
      (flatten (list (enumerate-path) (stumpwm::all-commands)))))))

;;; Returns T if there is a stumpwm command that matches 'command'
(defun stump-commandp (command)
  (loop for c in (stumpwm::all-commands) doing
	(if (string-equal command c)
	    (return-from stump-commandp t)))
  nil)

;;; Returns T if there is a .desktop file matching 'program'
;; Returns nil otherwise
(defun .desktopp (program)
  (let ((desktop-files
	 (concatenate
	  'list
	  (directory "/usr/share/applications/*.desktop")
	  (directory "/usr/local/share/applications/*.desktop")
	  (directory "~/.local/share/applications/*.desktop"))))
    (loop for p in desktop-files doing
	  (if (string-equal program
			    (first
			     (split-string
			      (car
			       (last
				(split-string
				 (namestring p) "/"))) ".")))
	      (return-from .desktopp t)))
    nil))

;;; Opens an instance of xterm and runs a commmand in it
;; The easiest way to keep xterm open after the command finishes would
;; be to slip a '; bash' at the end.  However, because
;; 'run-shell-command' is just a wrapper around sbcl's
;; 'sb-ext::run-program', which seems to escape special characters, this
;; does not work.  Currently, this passes the -hold flag, which tells
;; xterm not to exit on it's own; however, this does not create an
;; interactive shell, which would be preferable.  In most cases, this is
;; probably fine, since I usually pop open a terminal when I need to run
;; something cli-based.
(defun run-in-terminal (program)
  (run-shell-command
   (concatenate 'string "xterm -hold -e " program)))


;;; Takes in a name (string) and runs the program with that name
;; If the program is a gui program (determined by whether it has a
;; .desktop file), it will just launch the program. If it's not a gui
;; program, it will open a terminal and run it in that.  If it's a
;; stumpwm command, it will run that command.
;;
;; It will also allow for tab completion based on the $PATH environment
;; variable and the available stumpwm commands.
;;
;; Some steam games (specifically battle for wesnoth) have C-SPC bound
;; to in-game commands, so if the current group is steam and the window
;; is fullscreened, we'll just ignore this and send C-SPC instead.
(defcommand launcher () ()
  (cond ((and (current-window)
	      (string-equal
	       (format-expand *group-formatters* "%t" (current-group))
	       "steam")
	      (window-fullscreen (current-window)))
	 (stumpwm::send-fake-key (current-window)
				 (stumpwm::make-key
				  :control T :keysym 32)))
	(T
	 (let* ((program (completing-read (current-screen)
					  "run: "
					  (get-completions)))
		(short (separate-program program)))
	   (cond ((string-equal program "") nil)
		 ((stump-commandp short)
		  (stumpwm::eval-command program t))
		 ((.desktopp short) (run-shell-command program))
		 (T (run-in-terminal program)))))))

(define-key *top-map* (kbd "C-SPC") "launcher")
