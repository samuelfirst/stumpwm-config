;;; Backspace n chars
(defun backspace-n (n)
  (dotimes (x n)
    (run-shell-command "xdotool key BackSpace")))

;;; Replace selected text with a string
;; First need to de-select the text
;; Then backspace the string
;; Then send the new string with window-send-string
(defun replace-selection (str)
  (let* ((sel (get-x-selection))
	 (len (length sel)))
    (run-shell-command "xdotool key Left" t)
    (run-shell-command "xdotool key Right" t)
    (backspace-n len)
    (window-send-string str)))

;;; Macro to create selection commands and bind them to a key
;; The variable sel can be used in the command body for the selection
(defmacro make-selection-command (command key &body body)
  `(defcommand ,command () ()
     (let ((sel (get-x-selection)))
       (replace-selection ,@body)))
  `(define-key *root-map* ,key (format nil "~A" ',command)))


(make-selection-command captialize-selection (kbd "M-c")
			(string-capitalize sel))
(make-selection-command downcase-selection (kbd "M-l")
			(string-downcase sel))
(make-selection-command upcase-selection (kbd "M-u")
			(string-upcase sel))
