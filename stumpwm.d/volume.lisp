;;;; Volume.lisp provides code for interacting with alsa to do
;;;; things like get and set the current volume level.  It does
;;;; this by calling amixer and parsing the output.  Ideally, I'd
;;;; prefer to do this without calling amixer (by interacting with
;;;; files in /dev, /sys/class, or /proc), but alsa doesn't seem to
;;;; provide any sort of generic interface for getting the final mix
;;;; info.  I'm considering implementing something in C to do this,
;;;; but for now calling amixer is probably fine.


;;; Gets current volume and whether or not audio is muted
;; Returns results in a cons cell: (integer . boolean)
;; The line we're looking for in the output looks something like this:
;;  "Mono: Playback 54 [62%] [-24.75dB] [on]"
;; Parsing it is pretty simple, since we only need to look for the first
;; and last sets of brackets and take the info between them.
;; This can probably be made even simpler, since the line we need seems
;; to always be the last line, but I don't know enough about amixer to
;; confirm this.
(defun get-audio-information ()
  (let* ((command-out (run-shell-command "amixer sget Master" t))
	 (volume-line
	  (subseq command-out (search "Mono" command-out :from-end t)))
	 (volume (parse-integer
		  (subseq volume-line
			  (1+ (search "[" volume-line))
			  (1- (search "]" volume-line)))))
	 (muted (string-equal
		 "[off"
		 (subseq volume-line
			 (search "[" volume-line :from-end t)
			 (search "]" volume-line :from-end t)))))
    (cons volume muted)))

;;; Get the volume and return pretty string for use in the mode-line
(defun get-volume ()
  (let* ((info (get-audio-information))
	 (vol-str (cond ((cdr info) "off")
			(T (format nil "~A%" (car info))))))
    (progress-bar (concatenate 'string "volume: " vol-str)
		  (cond ((cdr info) 0)
			(T (car info))))))

;;; Print the volume as a message if the window is fullscreened
(defun print-fullscreen-volume ()
  (if (and (current-window) (window-fullscreen (current-window)))
	   (message (get-volume))))

;;; Set up the mute, vol down, & vol up keys.
(defcommand vol-mute () ()
  (print-fullscreen-volume)
  (run-shell-command "amixer -D pulse sset Master toggle"))

(defcommand vol-down () ()
  (print-fullscreen-volume)
  (run-shell-command "amixer sset Master 1-"))

(defcommand vol-up () ()
  (print-fullscreen-volume)
  (run-shell-command "amixer sset Master 1+"))

(define-key *top-map* (kbd "XF86AudioMute") "vol-mute")
(define-key *top-map* (kbd "XF86AudioLowerVolume") "vol-down")
(define-key *top-map* (kbd "XF86AudioRaiseVolume") "vol-up")
