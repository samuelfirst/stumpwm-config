;;; Set up command/shortcut for starting process monitor
(defcommand process-monitor () ()
  (run-shell-command "x-terminal-emulator -e htop"))

(define-key *top-map* (kbd "C-S-ESC") "process-monitor")


;;; Set up the print screen key
(defcommand screenshot () ()
  (run-shell-command "flameshot gui"))

(define-key *top-map* (kbd "Print") "screenshot")


;;; Kills a program, then reopens it
;; This is mainly for steam, since it has a nasty habit of not
;; recognizing when a game closes and needs a hard-restart.
(defcommand restart-program () ()
  (let ((win (current-window)))
    (kill-window win)
    (run-shell-command (string-downcase
			(window-class win)))))

(define-key *root-map* (kbd "K") "restart-program")
