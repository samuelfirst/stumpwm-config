;;; Sends ICMP packets to a host based on the hostname
;; Gets the ip of the host and passes it to ping-ip.
(defun ping-hostname (hostname)
  ())

;;; Sends an ICMP packet to a host based on the IP address
;; Return T if a connection can be established, or NIL if it can't
(defun ping-ip (ip)
  ())
