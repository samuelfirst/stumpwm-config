;;; Set up command/shortcut for locking the screen
;; Some fullscreen programs (mostly games) break when i3lock runs.
;; To get around this, we create and switch to a new temporary group
;; when locking the screen.  Then we block until i3lock returns,
;; kill the temporary group, and return to the previous group.
(defcommand lock-screen () ()
  (let ((old-group (current-group)))
    (gnew ".lock")
    (run-shell-command "i3lock -c 000000" t)
    (gkill)
    (stumpwm::switch-to-group old-group)))

(define-key *top-map* (kbd "s-l") "lock-screen")
