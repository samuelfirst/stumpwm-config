;;; Set up the media keys to interact with mpv
;; Play/pause, next, and previous key functions are obvious; the stop
;; key is less obvious. I've bound it to open a list of mpv commands.
(defun create-mpv-loop (command)
  (run-shell-command (concatenate 'string
				  "for s in /tmp/mpvsocket*; do echo "
				  command
				  " | nc -Uq 0 \"$s\"; done")))

(defcommand mpv-pause () ()
   (create-mpv-loop "'{\"command\": [\"cycle\", \"pause\"]}'"))

(defcommand mpv-playlist-next () ()
  (create-mpv-loop "'{\"command\": [\"playlist-next\"]}'"))

(defcommand mpv-playlist-prev () ()
  (create-mpv-loop "'{\"command\": [\"playlist-prev\"]}'"))

(defcommand mpv-command-list () ()
  (create-mpv-loop
   (cdr (select-from-menu
	 (current-screen)
	 '(("Play/Pause" . "'{\"command\": [\"cycle\", \"pause\"]}'")
	   ("Next Track" . "'{\"command\": [\"playlist-next\"]}'")
	   ("Prev Track" . "'{\"command\": [\"playlist-prev\"]}'")
	   ("Shuffle" . "'{\"command\": [\"playlist-shuffle\"]}'")
	   ("Unshuffle" . "'{\"command\": [\"playlist-unshuffle\"]}'")
	   ("Quit" . "'{\"command\": [\"quit\"]}'"))))))

(define-key *top-map* (kbd "XF86AudioPlay") "mpv-pause")
(define-key *top-map* (kbd "XF86AudioNext") "mpv-playlist-next")
(define-key *top-map* (kbd "XF86AudioPrev") "mpv-playlist-prev")
(define-key *top-map* (kbd "XF86AudioStop") "mpv-command-list")
