;;; Set up panic mode
;; Creates new group with normal windows in it, and switches to it.
;; To be used when doing something that you don't want someone else to
;; see.
(define-frame-preference ".panic"
  (0 t nil :class "Emacs")
  (1 t nil :class "Evince"))

(defcommand panic! () ()
  (gnew ".panic")
  (unless (group-windows (current-group))
    (hsplit)
    (run-shell-command
     "emacs --no-splash ~/classwork/Sec+/Thompson/chapter_6.org")
    (run-shell-command
     "evince ~/classwork/Sec+/Thompson/chapter_6.pdf"))
  (vol-mute))

(defcommand test () ()
  (message (stumpwm::find-group (current-screen) "Default")))

(define-key *top-map* (kbd "s-p") "panic!")
