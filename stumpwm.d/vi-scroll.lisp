(defcommand vi-scroll-up () ()
  (run-shell-command "xdotool key Up"))

(defcommand vi-scroll-down () ()
  (run-shell-command "xdotool key Down"))

(defcommand vi-scroll-left () ()
  (run-shell-command "xdotool key Left"))

(defcommand vi-scroll-right () ()
  (run-shell-command "xdotool key Right"))

;; Stumpwm v0.9.9 doesn't support define-interactive-keymap, so this
;; is used if it's not defined.  It doesn't support as many features,
;; but it sort of works as a poor-man's version.  At the moment, I
;; can't hold down the key and have it keep sending the command.
;; I'm considering using an input box with a char-limit of one and
;; launching it in a loop to get around this.
(defun inter-keymap-compat (keymaps)
  (defcommand disable-vi-scroll () ()
    (loop for map in keymaps doing
	  (undefine-key *top-map* (car map)))
    (undefine-key *top-map* (kbd "C-g"))
    (undefine-key *top-map* (kbd "C-v")))
  (defcommand vi-scroll-mode () ()
    (loop for map in keymaps doing
	  (define-key *top-map* (car map) (cadr map)))
    (define-key *top-map* (kbd "C-g") "disable-vi-scroll")
    (define-key *top-map* (kbd "C-v") "disable-vi-scroll")))

(if (fboundp 'define-interactive-keymap)
    (define-interactive-keymap vi-scroll-mode
      (:exit-on '((kbd "C-g") (kbd "C-v")))
      ((kbd "k") "vi-scroll-up")
      ((kbd "j") "vi-scroll-down")
      ((kbd "h") "vi-scroll-left")
      ((kbd "l") "vi-scroll-right"))
  (inter-keymap-compat (list (list (kbd "k") "vi-scroll-up")
			     (list (kbd "j") "vi-scroll-down")
			     (list (kbd "h") "vi-scroll-left")
			     (list (kbd "l") "vi-scroll-right"))))

(define-key *root-map* (kbd "C-v") "vi-scroll-mode")
