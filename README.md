# Stumpwm Config

This repo contains my run control files for stumpwm as well as shell scripts to
auto-populate a new setup.  Because this is a strictly personal project, you can
expect to find lots of buggy/half-complete/partially-broken/weird code, so caveat
emptor is in full effect.

I'm also not going to bother licensing any of this, since it's more hassle than
it's probably worth; you can treat the code here like it's in the public domain.

If you have any questions about this project, feel free to shoot me an
[email](mailto:samuelfirst@protonmail.com).